
prefix := /usr/local
bindir := $(prefix)/bin

all: aports-glmr

aports-glmr: main.go
	go build .

install:
	install -D -m755 aports-glmr $(DESTDIR)/$(bindir)/aports-glmr

clean:
	rm -f aports-glmr
