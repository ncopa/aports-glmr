/*
 * Copyright (c) 2020 Natanael Copa <ncopa@alpinelinux.org>
 * SPDX-License-Identifier: MIT
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"

	"github.com/xanzy/go-gitlab"
)

func searchAport(project, aport string, gl *gitlab.Client) {
	fmt.Println("Searching:", aport)
	state := "opened"
	opt := &gitlab.ListProjectMergeRequestsOptions{
		Search: &aport,
		State:  &state,
	}
	mrs, _, err := gl.MergeRequests.ListProjectMergeRequests(project, opt)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, mr := range mrs {
		fmt.Printf("%6d %s  %s  %s\n", mr.ID, mr.TargetBranch, mr.WebURL, mr.Title)
	}
}

func searchAports(project string, aports *[]string) {
	gl, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"), gitlab.WithBaseURL("https://gitlab.alpinelinux.org/api/v4"))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
		return
	}

	for _, val := range *aports {
		searchAport(project, val, gl)
	}
}

func currentAport() (string, error) {
	if _, err := os.Stat("APKBUILD"); err != nil {
		return "", err
	}

	dir, err := os.Getwd()
	if err != nil {
		return "", err
	}

	return path.Base(dir), nil
}

func main() {
	project := flag.String("project", "alpine/aports", "Name of the project")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "usage: %s [FLAGS] [APORTS]\n\n"+
			"The following flags are supported:\n\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()

	if flag.NArg() == 0 {
		aport, err := currentAport()
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		searchAports(*project, &[]string{aport})
	} else {
		aports := flag.Args()
		searchAports(*project, &aports)
	}
}
