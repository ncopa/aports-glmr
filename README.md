CLI tool to search aports merge requests in gitlab

## Building

To build run:

    make

To install run:

    make install

## Usage:

usage: aports-glmr [FLAGS] [APORS]

The following flags are supported:

  -project string
	Name of the project (default "alpine/aports")

